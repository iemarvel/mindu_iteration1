﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MindUV3.Models;

namespace MindUV3.Controllers
{
    public class ArtspacesController : Controller
    {
        private MindUV1_dbEntities db = new MindUV1_dbEntities();

        // GET: Artspaces
        public ActionResult Index(int page = 0)
        {
            const int pageSize = 10;
            var count = db.Artspaces.Count();
            var data = db.Artspaces.OrderBy(a => a.Id).Skip(page * pageSize).Take(pageSize).ToList();
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;
            return View(data);
            //return View(db.Artspaces.Take(5).ToList());
        }
        // GET: Artspaces/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artspaces artspaces = db.Artspaces.Find(id);
            if (artspaces == null)
            {
                return HttpNotFound();
            }
            return View(artspaces);
        }

        // GET: Artspaces/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Artspaces/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Organisation,Street,Suburb_Town,Postcode,Website,Rating")] Artspaces artspaces)
        {
            if (ModelState.IsValid)
            {
                db.Artspaces.Add(artspaces);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(artspaces);
        }

        // GET: Artspaces/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artspaces artspaces = db.Artspaces.Find(id);
            if (artspaces == null)
            {
                return HttpNotFound();
            }
            return View(artspaces);
        }

        // POST: Artspaces/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Organisation,Street,Suburb_Town,Postcode,Website,Rating")] Artspaces artspaces)
        {
            if (ModelState.IsValid)
            {
                db.Entry(artspaces).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(artspaces);
        }

        // GET: Artspaces/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artspaces artspaces = db.Artspaces.Find(id);
            if (artspaces == null)
            {
                return HttpNotFound();
            }
            return View(artspaces);
        }

        // POST: Artspaces/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Artspaces artspaces = db.Artspaces.Find(id);
            db.Artspaces.Remove(artspaces);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
