﻿CREATE TABLE [dbo].[Artspaces] (
    [Id]           INT           NOT NULL,
    [Organisation] VARCHAR (100) NOT NULL,
    [Street]       VARCHAR (100) NOT NULL,
    [Suburb/Town]  VARCHAR (50)  NOT NULL,
    [Postcode]     INT           NOT NULL,
    [Website]      VARCHAR (50)  NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

