﻿CREATE TABLE [dbo].[ArtImages]
(
	[imgId] INT NOT NULL PRIMARY KEY, 
    [imgPath] NVARCHAR(MAX) NOT NULL, 
    [artSpaceId] INT NOT NULL, 
    CONSTRAINT [FK_ArtImages_ToArtSpaces] FOREIGN KEY ([artSpaceId]) REFERENCES [Artspaces]([Id])
)
