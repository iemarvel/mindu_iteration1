﻿CREATE TABLE [dbo].[Sports]
(
    [SportId]           INT           NOT NULL,
    [Organisation] VARCHAR (100) NOT NULL,
    [Street]       VARCHAR (100) NOT NULL,
    [Suburb/Town]  VARCHAR (50)  NOT NULL,
    [Postcode]     INT           NOT NULL,
    [State]      VARCHAR (50)  NOT NULL,
	[Business Category] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([SportId] ASC)
)
